# Getting Started

### Command build

* mvn clean install -Dmaven.test.skip=true
* java -Dspring.profiles.active=test -jar alue5g-1.0.0.jar

### Docker

* docker-compose up -d --build
* docker-compose stop

