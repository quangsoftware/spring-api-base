FROM adoptopenjdk/openjdk8:ubi
RUN mkdir /opt/app
ARG JAR_FILE=target/alue5g-1.0.0.jar
COPY ${JAR_FILE} /opt/app/alue5g-1.0.0.jar