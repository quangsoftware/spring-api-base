package com.quangnd.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.quangnd.entity.User;
import com.quangnd.repository.UserRepository;

@Component
public class DataLoader implements ApplicationRunner {

  @Autowired
  UserRepository userRepository;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Override
  public void run(ApplicationArguments args) throws Exception {
    // // Khi chương trình chạy
    // // Insert vào csdl một user.
    // User user = new User();
    // user.setUsername("loda");
    // user.setPassword(passwordEncoder.encode("loda"));
    // userRepository.save(user);
    // System.out.println(user);

  }

}
