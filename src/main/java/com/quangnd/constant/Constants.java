package com.quangnd.constant;

public final class Constants {
  public static final String API_PATH = "/api";

  public static final long EXPIRATION_TIME = 259200000; // 3 day
  public static final String SECRET = "secret";
  public static final String HEADER_TOKEN = "Token";

  public static final int DELETED_FLAG = 1;
  public static final int NOT_DELETE_FLAG = 0;
  public static final int MAX_VALUE_NUMBER = 999999999;

  public static final String LANG_EN = "en";
  public static final String LANG_JP = "ja";

  public static final String AMS_API_CREATE_MESSAGE = "Created success";
  public static final String AMS_API_UPDATE_MESSAGE = "Updated success";
  public static final String AMS_API_DELETE_MESSAGE = "Deleted success";

}
