package com.quangnd.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quangnd.bean.ResultBean;
import com.quangnd.dto.CustomerInsertRequest;
import com.quangnd.dto.CustomerUpdateRequest;
import com.quangnd.service.CustomerService;

@RestController
@RequestMapping(value = "customers")
public class CustomerController {

  @Autowired
  CustomerService customerService;

  /**
   * @description get all customers from database
   * 
   * @return list customers
   */
  @GetMapping
  public ResponseEntity<ResultBean> getAllCustomer() {
    ResultBean resultBean = customerService.getAllCustomer();
    return new ResponseEntity<>(resultBean, HttpStatus.OK);
  }

  /**
   * @description get customer by id request param
   * 
   * @param id
   * @return customer if exist
   */
  @GetMapping("/{id}")
  public ResponseEntity<ResultBean> getCustomerById(@PathVariable Integer id) {
    ResultBean resultBean = customerService.getCustomerById(id);
    return new ResponseEntity<>(resultBean, HttpStatus.OK);
  }

  /**
   * @description update customer by request body
   * 
   * @param request
   * @return message if success
   */
  @PutMapping("/{id}")
  public ResponseEntity<ResultBean> updateCustomer(@PathVariable Integer id,
      @Valid @RequestBody CustomerUpdateRequest request) {
    request.setId(id);
    ResultBean resultBean = customerService.updateCustomer(request);
    return new ResponseEntity<>(resultBean, HttpStatus.OK);
  }

  /**
   * @description delete customer by id
   * 
   * @param id
   * @return message if success
   */
  @DeleteMapping("/{id}")
  public ResponseEntity<ResultBean> deleteCustomer(@PathVariable Integer id) {
    ResultBean resultBean = customerService.deleteCustomer(id);
    return new ResponseEntity<>(resultBean, HttpStatus.OK);
  }

  /**
   * @description create new customer by request body
   * 
   * @param request
   * @return message if success
   */
  @PostMapping
  public ResponseEntity<ResultBean> createCustomer(
      @Valid @RequestBody CustomerInsertRequest request) {
    ResultBean resultBean = customerService.insertCustomer(request);
    return new ResponseEntity<>(resultBean, HttpStatus.OK);
  }

}
