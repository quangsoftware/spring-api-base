package com.quangnd.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.quangnd.bean.ResultBean;
import com.quangnd.dto.LoginRequest;
import com.quangnd.dto.RandomStuff;
import com.quangnd.service.UserService;

@RestController
public class UserController {

  @Autowired
  private UserService userService;

  @PostMapping("/login")
  public ResponseEntity<ResultBean> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    ResultBean resultBean = userService.authenticateUser(loginRequest);
    return new ResponseEntity<>(resultBean, HttpStatus.OK);
  }

  // Api /api/random yêu cầu phải xác thực mới có thể request
  @PreAuthorize("hasAnyRole('ADMIN','USER')")
  @GetMapping("/random")
  public RandomStuff randomStuff() {
    return new RandomStuff("JWT Hợp lệ mới có thể thấy được message này");
  }



}
