package com.quangnd.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "customer")
@Getter
@Setter
@Where(clause = "is_delete = false")
public class Customer extends BaseEntity {

  @Id
  private int id;

  private String address;

  private String name;

  private String note;

  private String note1;

  private String note2;

  private String note3;

  private String type;

}
