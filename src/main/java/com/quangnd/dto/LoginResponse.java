package com.quangnd.dto;

import java.util.Date;
import lombok.Data;

@Data
public class LoginResponse {

  private String accessToken;
  private String tokenType = "Bearer";
  private Date createdTime;
  private Date expirationTime;

  public LoginResponse(String accessToken, Date createdTime, Date expirationTime) {
    this.accessToken = accessToken;
    this.createdTime = createdTime;
    this.expirationTime = expirationTime;
  }

}
