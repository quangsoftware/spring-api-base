package com.quangnd.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerUpdateRequest {

  private Integer id;

  @NotBlank(message = "Customer name can not be null")
  @Size(max = 200, message = "Customer name must be between 1 and 200 characters")
  private String name;

  @NotBlank(message = "Customer type can not be null")
  @Size(max = 45, message = "Customer type must be between 1 and 45 characters")
  private String type;

  @Size(max = 200, message = "Customer address less than 200 characters")
  private String address;

  private String note;

}
