package com.quangnd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.quangnd.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  /**
   * Get user by string param username
   * 
   * @param username
   * @return User object if exist
   */
  User findByUsername(String username);

}
