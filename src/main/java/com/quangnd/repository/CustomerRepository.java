package com.quangnd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.quangnd.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

  /**
   * Get customer by string param name
   * 
   * @param name
   * @return Customer object if exist
   */
  Customer findByName(String name);

  /**
   * Get customer by string param name, param id
   * 
   * @param name
   * @return Customer object if exist in another customer by name
   */
  Customer findByNameAndIdNot(String name, Integer id);

}
