package com.quangnd.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author QuangND
 */
public class DateUtil {

  public static final String YYYMMDD_HHMMSS = "yyyy-mm-dd hh:mm:ss";
  public static final String YYYMMDD = "yyyy/MM/dd";
  public static final String YYYYDDMM = "yyyy/dd/MM";
  public static final String DDMMYYY = "dd/MM/yyyy";
  public static final String YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
  public static final String MM_YYYY = "MM/yyyy";
  public static final String MMYYYY = "MMyyyy";

  public static Date convertDatePortal(String dateStr) throws ParseException {
    // 1982-02-06T00:00:00
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYMMDD_HHMMSS);
    return simpleDateFormat.parse(dateStr);
  }

  /**
   * Convert String to date
   * 
   * @param date
   * @return Date of input String
   * @throws ParseException
   */
  public static Date convertStringToDate(String date) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat(YYYMMDD);
    return dateFormat.parse(date);
  }

  public static Date convertDate(String dateStr, String format) throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
    return simpleDateFormat.parse(dateStr);
  }

  public static String convertDate(Date date, String format) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
    return simpleDateFormat.format(date);
  }

  public static Date currentDateEffort() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    return calendar.getTime();
  }

  public static int getDay(Date date) {
    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    int day = localDate.getDayOfMonth();
    return day;
  }

  public static int getMonth(Date date) {
    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    int month = localDate.getMonthValue();
    return month;
  }

  public static int getYear(Date date) {
    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    int month = localDate.getYear();
    return month;
  }

  public static String getStringOfMonthAndYear(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    int month = calendar.get(Calendar.MONTH) + 1;
    int year = calendar.get(Calendar.YEAR);
    return String.valueOf(month) + year;
  }

  public static boolean isValidDate(String date, String format) {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
    simpleDateFormat.setLenient(false);
    try {
      Date dateTmp = simpleDateFormat.parse(date);
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(dateTmp);
      return date.matches("^\\d+\\/\\d+\\/\\d{4}");
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * @author linhPV6956
   * @created 24/12/2018
   * 
   * @param date format by MM/yyyy
   * @param format
   * @return
   */
  public static boolean isValidDateMonthYear(String date, String format) {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
    simpleDateFormat.setLenient(false);
    try {
      Date dateTmp = simpleDateFormat.parse(date);
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(dateTmp);
      return date.matches("^\\d+\\/\\d{4}");
    } catch (Exception e) {
      return false;
    }
  }

  public static boolean isValidMonthAndYear(String date, String format) {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
    simpleDateFormat.setLenient(false);
    try {
      Date dateTmp = simpleDateFormat.parse(date);
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(dateTmp);
      return date.matches("^\\d{6}");
    } catch (Exception e) {
      return false;
    }
  }

  public static int compareMonthAndYear(Date date1, Date date2) {
    YearMonth ym1 = YearMonth.from(date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    YearMonth ym2 = YearMonth.from(date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    return ym1.compareTo(ym2);
  }
}
