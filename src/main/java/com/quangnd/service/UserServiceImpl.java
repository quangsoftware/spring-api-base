package com.quangnd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.quangnd.bean.ResultBean;
import com.quangnd.dto.LoginRequest;
import com.quangnd.dto.LoginResponse;
import com.quangnd.entity.CustomUserDetails;
import com.quangnd.entity.User;
import com.quangnd.jwt.JwtTokenProvider;
import com.quangnd.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenProvider tokenProvider;

  @Autowired
  private UserRepository userRepository;

  private ResultBean resultBean;

  @Override
  public UserDetails loadUserByUsername(String username) {
    User user = userRepository.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    }
    return new CustomUserDetails(user);
  }

  @Override
  public UserDetails loadUserById(Long userId) throws UsernameNotFoundException {
    User user = userRepository.findById(userId).orElse(null);
    if (user == null) {
      throw new UsernameNotFoundException(userId + "");
    }
    
    return new CustomUserDetails(user);
  }

  @Override
  public ResultBean authenticateUser(LoginRequest loginRequest) {
    resultBean = new ResultBean();
    // Xác thực từ username và password.
    Authentication authentication =
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
            loginRequest.getUsername(), loginRequest.getPassword()));

    // Nếu không xảy ra exception tức là thông tin hợp lệ
    // Set thông tin authentication vào Security Context
    SecurityContextHolder.getContext().setAuthentication(authentication);

    // Trả về jwt cho người dùng.
    LoginResponse loginResponse =
        tokenProvider.generateTokenObject((CustomUserDetails) authentication.getPrincipal());
    resultBean.setData(loginResponse);
    
    return resultBean;
  }

}
