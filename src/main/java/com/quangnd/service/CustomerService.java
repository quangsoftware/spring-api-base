package com.quangnd.service;

import com.quangnd.dto.CustomerInsertRequest;
import com.quangnd.dto.CustomerUpdateRequest;
import com.quangnd.bean.ResultBean;

/**
 * 
 * @author QuangND
 * @created 19/9/2018
 *
 */
public interface CustomerService {
  /**
   * Insert customer by request object
   * 
   * @param request
   * @return string message if success
   */
  ResultBean insertCustomer(CustomerInsertRequest request);

  /**
   * Update customer by request object
   * 
   * @param request
   * @return string message if success
   */
  ResultBean updateCustomer(CustomerUpdateRequest request);

  /**
   * Get customer by String id
   * 
   * @param id
   * @return customer object
   */
  ResultBean getCustomerById(Integer id);

  /**
   * Get all customer in database
   * 
   * @return list customer objects
   */
  ResultBean getAllCustomer();

  /**
   * Delete customer by String id
   * 
   * @param id
   * @return string message if success
   */
  ResultBean deleteCustomer(Integer id);
}
