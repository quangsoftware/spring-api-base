package com.quangnd.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quangnd.bean.ResultBean;
import com.quangnd.constant.Constants;
import com.quangnd.dto.CustomerInsertRequest;
import com.quangnd.dto.CustomerResponse;
import com.quangnd.dto.CustomerUpdateRequest;
import com.quangnd.entity.Customer;
import com.quangnd.exception.LogicErrorException;
import com.quangnd.repository.CustomerRepository;
import com.quangnd.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private ModelMapper mapper;

  @Autowired
  private CustomerRepository customerRepository;

  private ResultBean resultBean;

  @Override
  public ResultBean insertCustomer(CustomerInsertRequest request) {
    resultBean = new ResultBean();
    log.info("############ Begin insert customer. ##############");

    Customer customer = mapper.map(request, Customer.class);

    if (customerRepository.findByName(customer.getName()) == null) {
      customerRepository.save(customer);
    } else {
      log.error("Can not insert customer because the name is existed");
      throw new LogicErrorException("name", "Name is existed");
    }

    resultBean.setMessage("Created success");

    log.info("############ End insert customer. ##############");
    return resultBean;
  }

  @Override
  public ResultBean updateCustomer(CustomerUpdateRequest request) {
    resultBean = new ResultBean();
    log.info("############ Begin update customer. ##############");

    Integer id = request.getId();
    validateCustomerId(id);

    Customer updateCustomer = customerRepository.findById(id).orElse(null);

    if (updateCustomer == null) {
      log.error("Can not update customer because the customer is not exits");
      throw new LogicErrorException("id", "Customer id is not existed");
    }
    // can not update if the name is existed in another customer
    if (customerRepository.findByNameAndIdNot(request.getName(), id) != null) {
      log.error("Can not update customer because the name is existed in another customer");
      throw new LogicErrorException("name", "Customer name is existed");
    }

    updateCustomer = mapper.map(request, Customer.class);

    customerRepository.save(updateCustomer);

    resultBean.setMessage("Updated success");

    log.info("############ End update customer. ##############");
    return resultBean;
  }

  @Override
  public ResultBean getCustomerById(Integer id) {
    resultBean = new ResultBean();
    log.info("############ Begin get customer by id. ##############");
    // validate customer id
    validateCustomerId(id);
    // get customer by id
    Customer customer = customerRepository.findById(id).orElse(null);
    if (customer != null) {
      CustomerResponse customerResponse = mapper.map(customer, CustomerResponse.class);
      resultBean.setData(customerResponse);
    } else {
      log.error("Can not get customer of id: " + id);
      throw new LogicErrorException("id", "Customer id is not existed");
    }

    log.info("############ End get customer by id. ##############");
    return resultBean;
  }

  @Override
  public ResultBean getAllCustomer() {
    resultBean = new ResultBean();
    log.info("############ Begin get all customer. ##############");
    List<Customer> listCustomer = customerRepository.findAll();

    List<CustomerResponse> listCustomerResponse =
        ObjectMapperUtils.mapAll(listCustomer, CustomerResponse.class);

    resultBean.setData(listCustomerResponse);

    log.info("############ End get all customer. ##############");
    return resultBean;
  }

  @Override
  public ResultBean deleteCustomer(Integer id) {
    resultBean = new ResultBean();
    log.info("############ Begin delete customer by id. ##############");
    // validate id
    validateCustomerId(id);
    Customer delCustomer = customerRepository.findById(id).orElse(null);

    if (delCustomer == null) {
      log.error("Can not delete customer because the customer is not existed with id: " + id);
      throw new LogicErrorException("id", "Customer id is not existed");
    } else {
      delCustomer.setIsDelete(Constants.DELETED_FLAG);
      customerRepository.save(delCustomer);
    }

    resultBean.setMessage("Deleted success");
    log.info("############ End delete customer by id. ##############");

    return resultBean;
  }

  /**
   * @description validate role id
   * 
   * @param id
   */
  private void validateCustomerId(Integer id) {

    if (id <= 0 || id > Constants.MAX_VALUE_NUMBER) {
      log.error("########### Error with limit customer id ################");
      throw new LogicErrorException("id", "Error Id max value");
    }
  }

}
