package com.quangnd.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import com.quangnd.bean.ResultBean;
import com.quangnd.dto.LoginRequest;

public interface UserService extends UserDetailsService {

  UserDetails loadUserById(Long userId) throws UsernameNotFoundException;
  
  ResultBean authenticateUser(LoginRequest loginRequest);

}
